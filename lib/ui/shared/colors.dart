import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

class AppColors {
  static Color mainColorGrey = Color.fromRGBO(245, 245, 245, 1);
  static Color mainColorWhite = Color.fromRGBO(255, 255, 255, 1);
  static Color mainColorBlack = Color.fromRGBO(0, 0, 0, 1);
  static Color mainColorGreen = Color.fromRGBO(0, 197, 105, 1);
  static Color mainColorTextGrey = Color.fromRGBO(146, 146, 146, 1);
}
