import 'package:flutter/material.dart';
import 'package:shoopping/ui/shared/colors.dart';

class CustomText extends StatelessWidget {
  const CustomText({
    super.key,
    required this.content,
    this.colorText,
    this.fontSize,
    this.fontWeight,
    this.decoration,
    this.decorationThickness,
  });
  final String content;
  final Color? colorText;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextDecoration? decoration;
  final double? decorationThickness;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Text(
      content,
      style: TextStyle(
        color: colorText ?? AppColors.mainColorBlack,
        fontSize: fontSize ?? size.width * 0.045,
        fontWeight: fontWeight ?? FontWeight.w400,
        decoration: decoration ?? TextDecoration.none,
        decorationThickness: decorationThickness ?? 0,
      ),
    );
  }
}
