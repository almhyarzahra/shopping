import 'package:flutter/material.dart';
import 'package:shoopping/ui/shared/colors.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {super.key,
      this.Obscure,
      this.letterSpacing,
      this.fontWeight,
      required this.controller,
      this.validator});

  final bool? Obscure;
  final double? letterSpacing;
  final FontWeight? fontWeight;
  final TextEditingController controller;
  final String? Function(String?)? validator;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return TextFormField(
      validator: widget.validator,
      controller: widget.controller,
      textInputAction: TextInputAction.next,
      obscureText: widget.Obscure ?? false,
      style: TextStyle(
          letterSpacing: widget.letterSpacing ?? 0,
          fontWeight: widget.fontWeight ?? FontWeight.w400,
          color: AppColors.mainColorBlack,
          fontSize: size.width * 0.055),
      decoration: InputDecoration(
          errorStyle: TextStyle(color: AppColors.mainColorGreen),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.mainColorGreen))),
    );
  }
}
