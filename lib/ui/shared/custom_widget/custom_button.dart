import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shoopping/ui/shared/colors.dart';
import 'package:shoopping/ui/views/sign_in_view/sign_in_view.dart';

class CustomButton extends StatefulWidget {
  const CustomButton({
    super.key,
    required this.onPressed,
    required this.Text,
    this.backgroundColor,
    this.SvgName,
    this.TextColor,
  });
  final VoidCallback onPressed;
  final String Text;
  final Color? backgroundColor;
  final String? SvgName;
  final Color? TextColor;

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return OutlinedButton(
        onPressed: () {
          widget.onPressed();
        },
        style: OutlinedButton.styleFrom(
          backgroundColor: widget.backgroundColor ?? AppColors.mainColorGreen,
          fixedSize: Size(size.width * 0.85, size.height * 0.06),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (widget.SvgName != null) ...[
              Container(
                width: size.width * 0.08,
                child: SvgPicture.asset(
                  "images/${widget.SvgName}.svg",
                ),
              ),
              (size.width * 0.12).pw,
              Expanded(
                  child: Text(
                widget.Text,
                style: TextStyle(
                  color: widget.TextColor ?? AppColors.mainColorWhite,
                ),
              )),
            ] else
              Text(
                widget.Text,
                style: TextStyle(
                  color: widget.TextColor ?? AppColors.mainColorWhite,
                ),
              ),
          ],
        ));
  }
}
