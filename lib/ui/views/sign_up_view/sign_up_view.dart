import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shoopping/ui/shared/colors.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_button.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text_field.dart';
import 'package:shoopping/ui/shared/utils.dart';
import 'package:shoopping/ui/views/sign_in_view/sign_in_view.dart';
import 'package:shoopping/ui/views/verification_view/Verification_view.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({super.key});

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainColorGrey,
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Container(
                height: size.height * 0.1,
                color: AppColors.mainColorWhite,
                child: Padding(
                  padding: EdgeInsets.only(right: size.width * 0.85),
                  child: IconButton(
                    iconSize: size.width * 0.06,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back_ios),
                  ),
                )),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: size.width * 0.03, vertical: size.width * 0.08),
              child: Container(
                padding: EdgeInsets.all(size.width * 0.03),
                color: AppColors.mainColorWhite,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      content: "Sign Up",
                      fontSize: size.width * 0.1,
                      fontWeight: FontWeight.bold,
                    ),
                    (size.width * 0.15).ph,
                    CustomText(
                      content: "Name",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    CustomTextField(
                      controller: nameController,
                      validator: (value) {
                        return value!.isEmpty || !isName(value)
                            ? "Please Check Your Name"
                            : null;
                      },
                    ),
                    (size.width * 0.1).ph,
                    CustomText(
                      content: "Email",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    CustomTextField(
                      controller: emailController,
                      validator: (value) {
                        return value!.isEmpty || !isEmail(value)
                            ? "Please Chcek Your Email"
                            : null;
                      },
                    ),
                    (size.width * 0.1).ph,
                    CustomText(
                      content: "Password",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    CustomTextField(
                      controller: passwordController,
                      fontWeight: FontWeight.bold,
                      letterSpacing: size.width * 0.05,
                      Obscure: true,
                      validator: (value) {
                        return value!.isEmpty || !isPassword(value)
                            ? "Please Check Your Password"
                            : null;
                      },
                    ),
                    (size.width * 0.1).ph,
                    isLoading
                        ? SpinKitCircle(
                            color: AppColors.mainColorGreen,
                          )
                        : Center(
                            child: CustomButton(
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    Future.delayed(Duration(seconds: 3))
                                        .then((value) => {
                                              setState(() {
                                                isLoading = false;
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          VerificationView(),
                                                    ));
                                              })
                                            });
                                  }
                                },
                                Text: "SIGN UP"))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
