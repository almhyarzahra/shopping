import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shoopping/ui/shared/colors.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_button.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text_button.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text_field.dart';
import 'package:shoopping/ui/shared/utils.dart';
import 'package:shoopping/ui/views/reset_password_view/reset_password_view.dart';
import 'package:shoopping/ui/views/sign_up_view/sign_up_view.dart';

class SignInView extends StatefulWidget {
  const SignInView({super.key});

  @override
  State<SignInView> createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainColorGrey,
      // appBar: AppBar(
      //   toolbarHeight: size.height * 0.12,
      //   backgroundColor: AppColors.mainColorWhite,
      // ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Container(
              height: size.height * 0.1,
              color: AppColors.mainColorWhite,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: size.width * 0.03, vertical: size.width * 0.02),
              child: Container(
                padding: EdgeInsets.all(size.width * 0.03),
                color: AppColors.mainColorWhite,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomText(
                          content: "Welcome,",
                          fontSize: size.width * 0.1,
                          fontWeight: FontWeight.bold,
                        ),
                        Column(
                          children: [
                            (size.width * 0.07).ph,
                            CustomTextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => SignUpView(),
                                    ));
                              },
                              text: "Sign Up",
                              fontWeight: FontWeight.bold,
                              colorText: AppColors.mainColorGreen,
                            ),
                          ],
                        ),
                      ],
                    ),
                    (size.width * 0.03).ph,
                    CustomText(
                      content: "Sign in to Continue",
                      colorText: AppColors.mainColorTextGrey,
                    ),
                    (size.width * 0.14).ph,
                    CustomText(
                      content: "Email",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    CustomTextField(
                      controller: emailController,
                      validator: (value) {
                        return value!.isEmpty || !isEmail(value)
                            ? "Please Check Your Email"
                            : null;
                      },
                    ),
                    (size.width * 0.08).ph,
                    CustomText(
                      content: "Password",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    CustomTextField(
                      controller: passwordController,
                      Obscure: true,
                      validator: (value) {
                        return value!.isEmpty || !isPassword(value)
                            ? "Please Check Your Password"
                            : null;
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CustomTextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ResetPasswordView(),
                                ));
                          },
                          text: "Forgot Password?",
                          fontSize: size.width * 0.04,
                        ),
                      ],
                    ),
                    (size.width * 0.005).ph,
                    isLoading
                        ? SpinKitCircle(
                            color: AppColors.mainColorGreen,
                          )
                        : Center(
                            child: CustomButton(
                              onPressed: () {
                                _formKey.currentState!.validate()
                                    ? {
                                        print('ok'),
                                        setState(() {
                                          isLoading = true;
                                        }),
                                        Future.delayed(Duration(seconds: 3))
                                            .then((value) => {
                                                  setState(() {
                                                    isLoading = false;
                                                  })
                                                })
                                      }
                                    : print(':0');
                              },
                              Text: "SIGN IN",
                            ),
                          ),
                  ],
                ),
              ),
            ),
            (size.width * 0.05).ph,
            Center(child: CustomText(content: "-OR-")),
            (size.width * 0.07).ph,
            Center(
              child: CustomButton(
                onPressed: () {},
                SvgName: "ic_facebook",
                Text: "Sign in with Facebook",
                backgroundColor: AppColors.mainColorGrey,
                TextColor: AppColors.mainColorBlack,
              ),
            ),
            (size.width * 0.07).ph,
            Center(
              child: CustomButton(
                onPressed: () {},
                SvgName: "ic_googleColor",
                Text: "  Sign in with Google",
                backgroundColor: AppColors.mainColorGrey,
                TextColor: AppColors.mainColorBlack,
              ),
            ),
            (size.width * 0.07).ph,
          ],
        ),
      ),
    ));
  }
}

extension EmptyPadding on num {
  SizedBox get ph => SizedBox(height: toDouble());
  SizedBox get pw => SizedBox(width: toDouble());
}
