import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shoopping/ui/shared/colors.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_button.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text.dart';
import 'package:shoopping/ui/views/sign_in_view/sign_in_view.dart';

class VerificationView extends StatefulWidget {
  const VerificationView({super.key});

  @override
  State<VerificationView> createState() => _VerificationViewState();
}

class _VerificationViewState extends State<VerificationView> {
  TextEditingController number1Controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainColorGrey,
      body: ListView(
        children: [
          Container(
              height: size.height * 0.1,
              color: AppColors.mainColorWhite,
              child: Padding(
                padding: EdgeInsets.only(right: size.width * 0.85),
                child: IconButton(
                  iconSize: size.width * 0.06,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_ios),
                ),
              )),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: size.width * 0.03, vertical: size.width * 0.08),
            child: Container(
              padding: EdgeInsets.all(size.width * 0.03),
              color: AppColors.mainColorWhite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    content: "Verification",
                    fontSize: size.width * 0.08,
                    fontWeight: FontWeight.bold,
                  ),
                  (size.width * 0.04).ph,
                  CustomText(
                    content:
                        "A 6 - Digit PIN has been sent to your email\naddress, enter it below to continue",
                    colorText: AppColors.mainColorTextGrey,
                    fontSize: size.width * 0.04,
                  ),
                  (size.width * 0.2).ph,
                  Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        // margin:
                        //     EdgeInsets.symmetric(horizontal: size.width * 0.03),
                        child: PinCodeTextField(
                          keyboardType: TextInputType.number,
                          appContext: context,
                          controller: number1Controller,
                          length: 6,
                          cursorHeight: 18,
                          enableActiveFill: true,
                          textStyle: TextStyle(fontSize: size.width * 0.05),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            fieldWidth: size.width * 0.12,
                            fieldHeight: size.width * 0.12,
                            inactiveColor: Colors.grey,
                            selectedColor: Colors.lightBlue,
                            activeColor: Colors.blue,
                            selectedFillColor: Colors.blue,
                            inactiveFillColor: Colors.grey.shade100,
                            borderWidth: 1,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          onChanged: ((value) {}),
                        ),
                      ),
                      (size.width * 0.2).ph,
                      CustomButton(onPressed: () {}, Text: "CONTINUE"),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
