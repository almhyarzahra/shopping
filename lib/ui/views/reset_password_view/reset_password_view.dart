import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shoopping/ui/shared/colors.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_button.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text.dart';
import 'package:shoopping/ui/shared/custom_widget/custom_text_field.dart';
import 'package:shoopping/ui/shared/utils.dart';
import 'package:shoopping/ui/views/sign_in_view/sign_in_view.dart';
import 'package:shoopping/ui/views/verification_view/verification_view.dart';

class ResetPasswordView extends StatefulWidget {
  const ResetPasswordView({super.key});

  @override
  State<ResetPasswordView> createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  TextEditingController emailController = TextEditingController();
  GlobalKey<FormState> _foemKey = GlobalKey<FormState>();
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainColorGrey,
      body: Form(
        key: _foemKey,
        child: ListView(
          children: [
            Container(
                height: size.height * 0.1,
                color: AppColors.mainColorWhite,
                child: Padding(
                  padding: EdgeInsets.only(right: size.width * 0.85),
                  child: IconButton(
                    iconSize: size.width * 0.06,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back_ios),
                  ),
                )),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: size.width * 0.03, vertical: size.width * 0.08),
              child: Container(
                padding: EdgeInsets.all(size.width * 0.03),
                color: AppColors.mainColorWhite,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      content: "Reset Password",
                      fontSize: size.width * 0.08,
                      fontWeight: FontWeight.bold,
                    ),
                    (size.width * 0.08).ph,
                    CustomText(
                      content:
                          "Please enter your email to receive a\nlink to create a new password via email",
                      colorText: AppColors.mainColorTextGrey,
                      fontSize: size.width * 0.04,
                    ),
                    (size.width * 0.08).ph,
                    CustomText(
                      content: "Email",
                      colorText: AppColors.mainColorTextGrey,
                    ),
                    CustomTextField(
                      controller: emailController,
                      validator: (value) {
                        return value!.isEmpty || !isEmail(value)
                            ? "Please Check Your Email"
                            : null;
                      },
                    ),
                    (size.width * 0.08).ph,
                    isLoading
                        ? SpinKitCircle(
                            color: AppColors.mainColorGreen,
                          )
                        : CustomButton(
                            onPressed: () {
                              if (_foemKey.currentState!.validate()) {
                                setState(() {
                                  isLoading = true;
                                });
                                Future.delayed(Duration(seconds: 3))
                                    .then((value) => {
                                          setState(() {
                                            isLoading = false;
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      VerificationView(),
                                                ));
                                          })
                                        });
                              }
                            },
                            Text: "SEND"),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
